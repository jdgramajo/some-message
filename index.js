const express = require('express');
const app = express();
const messageRoutes = require('./routes/message');
const port = 3000;

app.use(express.json());

app.use('/message', messageRoutes);

app.use('/', (req, res, next) => {
  res.status(405).end();
})

app.listen(port, () => {
  console.log(`App running in http://localhost:${port}`)
})
