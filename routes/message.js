const router = require('express').Router();

let message = "";

router.use((req, res, next) => {
  if (req.path === '/') {
    switch (req.method) {
      case 'GET':
        res.status(200).json({ message });
        break;
      case 'POST':
        const reqBodyKeys = req.body && Object.keys(req.body);
        if ( reqBodyKeys && reqBodyKeys.length === 1 && reqBodyKeys[0] === 'message' ) {
          message = req.body[reqBodyKeys];
          res.status(200).send('Message stored in memory.');
          break;
        } else {
          res.status(422).end();
          break;
        }
      default:
        next();
    }
  } else {
    next();
  }
});

module.exports = router;
