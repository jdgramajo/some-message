const express = require('express');
const messageRoutes = require('../../routes/message');

const supertest = require('supertest');
const { expect } = require('chai');

describe('Good tests on /message endpoint', () => {
  let app;
  let request;

  before(() => {
    app = express();
    app.use(express.json());

    app.use('/message', messageRoutes);

    request = supertest(app);
  });

  it('First GET to /message resolves 200 status code, no message string', (done) => {
    request
      .get('/message')
      .expect(200, (err, res) => {
        expect(res.body.message).to.equal('');
        done();
      })
  });

  it('A bad POST body returns 422', (done) => {
    request
      .post('/message')
      .set('Content-Type', 'application/json')
      .send('{"bad": "object"}')
      .expect(200, (err, res) => {
        expect(res.statusCode).to.equal(422);
        done();
      });
  });

  it('A POST with no body data type returns 422', (done) => {
    request
      .post('/message')
      .send('{"message": "Hello buddy!"}')
      .expect(200, (err, res) => {
        expect(res.statusCode).to.equal(422);
        done();
      });
  });

  it('A POST with empty body returns 422', (done) => {
    request
      .post('/message')
      .set('Content-Type', 'application/json')
      .expect(200, (err, res) => {
        expect(res.statusCode).to.equal(422);
        done();
      });
  });

  it('POST resolves 200 status code and indicates message was stored', (done) => {
    request
      .post('/message')
      .set('Content-Type', 'application/json')
      .send('{"message": "Hello buddy!"}')
      .expect(200, (err, res) => {
        expect(res.text).to.equal('Message stored in memory.');
        expect(res.statusCode).to.equal(200);
        done();
      });
  });

  it('A GET after a successful POST returns the currently stored message', (done) => {
    request
      .get('/message')
      .expect(200, (err, res) => {
        expect(res.body.message).to.equal('Hello buddy!');
        done();
      })
  });

});
